package com.classpath.orders.event;

public enum OrderStatus {
	
	ORDER_FULFILLED,
	ORDER_DECLINED,
	ORDER_ACCEPTED,
	ORDER_CANCELLED

}

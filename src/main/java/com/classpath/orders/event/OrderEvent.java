package com.classpath.orders.event;

import java.time.LocalDateTime;

import com.classpath.orders.model.Order;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;

@AllArgsConstructor
@Builder
@Getter
public class OrderEvent {
	private LocalDateTime eventTime;
	private OrderStatus orderStatus;
	private Order order;

}
